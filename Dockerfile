FROM liliancal/ubuntu-php-apache
COPY /src /var/www
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]